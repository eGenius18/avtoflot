<?php include_once("config.php");?>
<?php include_once('functions/mail_check.php'); ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=META_DESCRIPTION;?>">
    <title><?=META_TITLE;?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<?php
if (@$_GET['test']=='test') :?>
    <div class="container" style="background-color:green; z-index:100; position: fixed; height: 25px;">
        <div class="row">
            <div class="col boot-xs visible-xs">xs</div>
            <div class="col boot-sm visible-sm">sm</div>
            <div class="col boot-md visible-md">md</div>
            <div class="col boot-lg visible-lg">lg</div>
        </div>
    </div>
<?php
endif;
?>

<div class="container header">
    <header class="row">
        <div class="col-lg-7 col-sm-4 header__logo">
            <img src="img/logo.png" class="logo" alt="Автофлот - сеть автопарков такси">
            <span><?=LOGO_SLOGAN;?></span>
        </div>
        <div class="col-lg-5 col-sm-8 header__right header__xs">
            <span class="header__slogan">Звонок по РФ бесплатный</span>
            <span class="header__phone"><a href="tel:<?=PHONE_LINK;?>"><?=PHONE;?></a></span>
            <span class="header__hamburger"></span>
        </div>
    </header>
    <div class="row visible-lg"><div class="col-xs-12"><hr></div></div>
    <nav class="row visible-lg nav">
        <div class="col-xs-9">
            <ul class="clearfix">
                <li><a href="#main_calc">Расчет доходности</a></li>
                <li><a href="#main_garant">Гарантии</a></li>
                <li><a href="#main_schem">Схема работы</a></li>
                <li><a href="#main_faq">Ответы на вопросы</a></li>
                <li><a href="#main_contacts">Контакты</a></li>
            </ul>
        </div>
        <div class="col-xs-3 header__presentation paddL0">
            <img src="img/icons-8-document.png" class="icons8-document">
            <a href="#modal_callback" data-fancybox>Презентация для инвесторов</a>
        </div>
    </nav>
</div>


<div class="teaser">
    <div class="teaser__gradient"></div>
    <div class="container padd0">
        <div class="col-lg-9 col-sm-12">
            <h2>Инвестируй в автомобили <span class="nobr">Яндекс-такси</span> и гарантированно получай от 20% годовых</h2>
            <ul>
                <li>Официальный договор с "Яндекс Такси"</li>
                <li>Обеспечение залога автомобилями</li>
                <li>Ежемесячная выплата процентов</li>
            </ul>
        </div>
    </div>
</div>


<div class="getpresent">
    <div class="container">
        <div class="row">
            <div class="col-xs-12"><h2>Получите презентацию для инвесторов и график выплат</h2></div>
        </div>
        <div class="row">
            <form action="">
                <div class="col-lg-3 col-sm-12"><input type="text" placeholder="Ваше имя"></div>
                <div class="col-lg-3 col-sm-12"><input type="text" placeholder="Ваш телефон"></div>
                <div class="col-lg-3 col-sm-12"><input type="text" placeholder="Ваш e-mail"></div>
                <div class="col-lg-3 col-sm-12"><input type="submit" value="ПОЛУЧИТЬ МАТЕРИАЛЫ" class="button"></div>
            </form>
        </div>
        <div class="row">
            <div class="col-xs-12"><p>Нажимая на кнопку «Получить материалы», вы акцептуете <a href="">оферту</a> и соглашаетесь на обработку <a href="">персональных данных</a></p></div>
        </div>
    </div>
</div>



<div class="container who">
    <div class="row">
        <div class="col-xs-12"><h2>Кто сказал что в России нет бизнесов, которые выгодны и безопасны для инвесторов</h2></div>
    </div>
    <div class="row">
        <div class="col-xs-12 visible-xs who__margin-xs">
            <img src="img/dir640.jpg"  class="img-responsive hidden-lg">
        </div>

        <div class="col-lg-6 col-sm-7 col-xs-12 who__margin-text">
            <p>Уже 12 лет я занимаюсь сферой салонного бизнеса. Все началось с того, что вокруг никто не мог предложить те услуги и тот сервис, который был нужен людям. С этого мы и начали.</p>
            <p>В настоящее время мы с командой открыли более 110 салонов по всей России. Их число постоянно растет и каждый раз мы спрашиваем: «В чем нуждаются люди?»</p>
            <p>Именно этот вопрос помогает нам держать руку на пульсе, улучшая качество услуг и уровень сервиса. Мы с большим уважением и заботой относимся к каждому клиенту, и если благодаря нам люди становятся чуточку счастливее, значит мы не зря делаем свою работу!</p>
            <p style="margin: 50px 0 40px"><b>Александр Сергеев<br>Директор компании «Автофлот»</b></p>
            <p class="button__wrap visible-lg"><a class="button" href="#modal_callback" data-fancybox>стать нашим партнером</a></p>
        </div>
        <div class="col-lg-5 col-lg-offset-1 col-sm-5 hidden-xs who__margin-img">
<!--            <img src="img/layer-85.jpg"  class="img-responsive visible-lg">-->
            <img src="img/dir640.jpg"  class="img-responsive hidden-lg">
        </div>
        <div class="col-xs-12">
            <p class="button__wrap hidden-lg"><a class="button" href="#modal_callback" data-fancybox>стать нашим партнером</a></p>
        </div>
    </div>
</div>




<div class="calc" id="main_calc">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 calc__padd0">
                <h2>Рассчитайте доходность ваших инвестиций за 2 простых шага</h2>
                <div class="calc__container">
                    <div class="container">
                        <div class="calc__padding">
                            <div class="col-lg-5 col-dm-12 calc__slider1">
                                <div class="calc__input">1. Выберите сумму инвестиций</div>
                                <div id="slider1"></div>
                                <div class="calc__slider-desc" id="slider1-out">
                                    <span class="sum1">690000</span> руб. (<span class="num1">1</span> <span class="sklon_auto">автомобиль</span>)
                                </div>
                            </div>
                            <div class="col-lg-5 col-lg-offset-2 col-sm-12 calc__slider2">
                                <div class="calc__input">2. Укажите срок инвестиций</div>
                                <div id="slider2"></div>
                                <div class="calc__slider-desc" id="slider2-out">
                                    <span class="num2">36</span> <span class="sklon_months">месяцев</span>
                                </div>
                            </div>



                            <div class="visible-lg">
                                <div class="col-sm-12" style="text-align: center"><div class="calc__head">Показатели доходности</div></div>
                                <div class="col-sm-3">
                                    <div class="calc__dohod-head calc__dohod-subhead1">Сумма ваших инвестиций:</div>
                                    <div class="calc__dohod-sum"><span id="dohod1">690 000</span> руб.</div>
                                </div>
                                <div class="col-sm-3 calc__arrow">
                                    <div class="calc__dohod-head calc__dohod-subhead2">Ежемесячный доход:</div>
                                    <div class="calc__dohod-sum"><span id="dohod2">22 000</span> руб.</div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="calc__dohod-head calc__dohod-subhead3">Доход за <span id="dohod33">36</span> <span class="sklon_months2">месяцев</span>:</div>
                                    <div class="calc__dohod-sum"><span id="dohod3">792 000</span> руб.</div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="calc__dohod-head calc__dohod-subhead4">Возврат после продажи машин:</div>
                                    <div class="calc__dohod-sum"><span id="dohod4">360 000</span> руб.</div>
                                </div>
                            </div>
                            <div class="hidden-lg">показатели доходности</div>



                            <div class="col-sm-12" style="text-align: center">
                                <div class="calc__head2">ваш доход</div>
                            </div>
                            <div class="col-sm-12" style="text-align: center">
                                <div class="calc__total"><span id="dohod5">1 152 000</span> руб.</div>
                            </div>
                        </div>


                        <div class="col-xs-12 calc__return"><a href="#modal_return" data-fancybox>досрочный возврат инвестиций</a></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="container besure" id="main_garant">
    <div class="row">
        <div class="col-xs-12">
            <h2>Будьте уверены в сохранности Ваших вложений <div>Все гарантии прописаны в договоре</div></h2>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 visible-xs">
            <img src="img/group-4.png" class="img-responsive">
        </div>
        <div class="col-sm-6 col-xs-12">
            <h3>Инвестиции защищены залогом</h3>
            <p>В залоге у инвестора находится автомобиль, купленный на инвестиционные средства.</p>
            <p>При форс-мажорных обстоятельствах Вы получите его обратно, сохранив деньги.</p>
        </div>
        <div class="col-sm-6 hidden-xs">
            <img src="img/group-4.png" class="img-responsive">
        </div>
    </div>
    <div class="row"><div class="col-xs-12"><hr></div></div>



    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <img src="img/icons-8-search-1.png" class="img-responsive">
        </div>
        <div class="col-sm-6 col-xs-12">
            <h3>Ежемесячные выплаты на счет</h3>
            <p>Выплаты процентов производятся ежемесячно, путем перевода средств на счет физического лица.</p>
            <p>Даты и суммы платежей указаны в графике выплат.</p>
        </div>
    </div>
    <div class="row"><div class="col-xs-12"><hr></div></div>



    <div class="row">
        <div class="col-xs-12 visible-xs">
            <img src="img/group-6.png" class="img-responsive">
        </div>
        <div class="col-sm-6 col-xs-12">
            <h3>Возможность досрочного возврата средств</h3>
            <p>В договоре предусмотрен досрочный возврат средств. Даже при минимальном сроке Вы окупите инвестиции и получите доход.</p>
            <p>График досрочного возврата - в приложении к договору.</p>
        </div>
        <div class="col-sm-6 hidden-xs">
            <img src="img/group-6.png" class="img-responsive">
        </div>
    </div>
    <div class="row"><div class="col-xs-12"><hr></div></div>



    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <img src="img/group-7.png" class="img-responsive">
        </div>
        <div class="col-sm-6 col-xs-12">
            <h3>Замена автомобилей при авариях и угонах</h3>
            <p>При аварии или угоне мы заменим Ваш автомобиль на другой, равноценный по стоимости, без простоев и доплат.</p>
            <p>Новый залог оформляется в дополнительном соглашении к договору.</p>
        </div>
    </div>



</div>



<div class="getdoc">
    <div class="container">
        <div class="row">
            <div class="col-xs-12"><h2>Получите полную версию договора на ваш e-mail</h2></div>
        </div>
        <div class="row">
            <form action="">
                <div class="col-lg-3 col-sm-12"><input type="text" placeholder="Ваше имя"></div>
                <div class="col-lg-3 col-sm-12"><input type="text" placeholder="Ваш телефон"></div>
                <div class="col-lg-3 col-sm-12"><input type="text" placeholder="Ваш e-mail"></div>
                <div class="col-lg-3 col-sm-12 getdoc__padd"><a type="submit" class="button yellow">получить материалы</a></div>
            </form>
        </div>
        <div class="row">
            <div class="col-xs-12"><p>Нажимая на кнопку «Получить материалы», вы акцептуете <a href="">оферту</a> и соглашаетесь на обработку <a href="">персональных данных</a></p></div>
        </div>
    </div>
</div>



<div class="compare visible-lg">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Почему инвестиции в автомобили "Яндекс Такси" выгоднее других направлений?</h2>

                <?php //изврат в виде обертки нужен изза бордер-радиуса - иначе одиночных границ не получится?>
                <div class="compare__wrap visible-lg visible-md">
                    <div class="compare__table panel-group" id="accordion">
                        <div>
                            <div>&nbsp;</div>
                            <div>Инвестиции в недвижимость</div>
                            <div>Инвестиции в автомобили <span class="nobr">"Яндекс Такси"</span></div>
                            <div>Собственный бизнес/франшиза</div>
                        </div>
                        <div>
                            <div>Срок запуска<br><a href="#" class="rowOpener" data-row="rowNum1">Подробнее</a></div>
                            <div>30-60 дней<br><div class="row rowNum1">На покупку и оснащение автомобиля уходит 3-5 дней, после этого он сразу начинает работу</div></div>
                            <div>2-5 дней<br><div class="row rowNum1">На покупку и оснащение автомобиля уходит 3-5 дней, после этого он сразу начинает работу</div></div>
                            <div>более 60 дней<br><div class="row rowNum1">На покупку и оснащение автомобиля уходит 3-5 дней, после этого он сразу начинает работу</div></div>
                        </div>
                        <div>
                            <div>Сумма инвестиций<br><a href="#" class="rowOpener" data-row="rowNum2">Подробнее</a></div>
                            <div>от 1 800 000 руб.<br><div class="row rowNum2">xxx</div></div>
                            <div>от 690 000 руб.<br><div class="row rowNum2">xxx</div></div>
                            <div>от 400 000 руб.<br><div class="row rowNum2">xxx</div></div>
                        </div>
                        <div>
                            <div>Годовая прибыль<br><a href="#" class="rowOpener" data-row="rowNum3">Подробнее</a></div>
                            <div>5-15%<br><div class="row rowNum3">xxx</div></div>
                            <div>20%<br><div class="row rowNum3">xxx</div></div>
                            <div>>20%<br><div class="row rowNum3">xxx</div></div>
                        </div>
                        <div>
                            <div>Риски<br><a href="#" class="rowOpener" data-row="rowNum4">Подробнее</a></div>
                            <div>Низкие<br><div class="row rowNum4">xxx</div></div>
                            <div>Низкие<br><div class="row rowNum4">xxx</div></div>
                            <div>Высокие<br><div class="row rowNum4">xxx</div></div>
                        </div>
                        <div>
                            <div>Вовлеченность в бизнес<br><a href="#" class="rowOpener" data-row="rowNum5">Подробнее</a></div>
                            <div>Требуется<br><div class="row rowNum5">xxx</div></div>
                            <div>Не требуется<br><div class="row rowNum5">xxx</div></div>
                            <div>Требуется<br><div class="row rowNum5">xxx</div></div>
                        </div>
                        <div>
                            <div>Досрочный вывод средств<br><a href="#" class="rowOpener" data-row="rowNum6">Подробнее</a></div>
                            <div>Медленный<br><div class="row rowNum6">xxx</div></div>
                            <div>Быстрый<br><div class="row rowNum6">xxx</div></div>
                            <div>Медленный<br><div class="row rowNum6">xxx</div></div>
                        </div>
                        <div>
                            <div></div>
                            <div></div>
                            <div><p class="button__wrap"><a class="button" href="#modal_callback" data-fancybox>узнать подробности</a></p></div>
                            <div></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
$rus_months = explode("|", '|января|февраля|марта|апреля|мая|июня|июля|августа|сентября|октября|ноября|декабря');

for ($i=1; $i<=5; $i++) {
    $now = time();
    switch ($i) {
        case 2: $now += 60*60*24; break;
        case 3: $now += 60*60*24*2; break;
        case 4: $now += 60*60*24*10; break;
        case 5: $now += 60*60*24*40; break;
    }
    $date[$i] = (object)[
            'day' => date("d", $now),
            'month' => $rus_months[date("m", $now)]
    ];
}

?>

<div class="profit container" id="main_schem">
    <div class="row">
        <div class="col-xs-12">
            <h2>1 месяц до получения первой прибыли</h2>
            <div class="table">

                <div class="tr right">
                    <div class="td">
                        <div class="date-xs"><div class="date"><?=$date[1]->day;?> <div><?=$date[1]->month;?></div></div></div>
                        <h4>Первые шаги</h4>
                        <ul>
                            <li><span>Первичная консультация со специалистом</span></li>
                            <li><span>Выбор оптимальной суммы для инвестирования</span></li>
                            <li><span>Отправка инвестиционного договора и презентации для ознакомления</span></li>
                        </ul>
                    </div>
                    <div class="td hidden-xs">&nbsp;</div>
                </div>

                <div class="tr left">
                    <div class="td hidden-xs">&nbsp;</div>
                    <div class="td">
                        <div class="date-xs"><div class="date"><?=$date[2]->day;?> <div><?=$date[2]->month;?></div></div></div>
                        <h4>Оформление документов</h4>
                        <ul>
                            <li><span>Повторная консультация по оставшимся вопросам</span></li>
                            <li><span>Заключение инвестиционного договора</span></li>
                            <li><span>Перевод инвестиций на расчетный счет</span></li>
                        </ul>
                    </div>
                </div>

                <div class="tr right">
                    <div class="td">
                        <div class="date-xs"><div class="date"><?=$date[3]->day;?> <div><?=$date[3]->month;?></div></div></div>
                        <h4>Покупка автомобиля</h4>
                        <ul>
                            <li><span>Покупка автомобиля и доп. оборудования</span></li>
                            <li><span>Оформление всех необходимых документов</span></li>
                            <li><span>Заключение доп.соглашения о залоге</span></li>
                        </ul>
                    </div>
                    <div class="td hidden-xs">&nbsp;</div>
                </div>

                <div class="tr left">
                    <div class="td hidden-xs">&nbsp;</div>
                    <div class="td">
                        <div class="date-xs"><div class="date"><?=$date[4]->day;?> <div><?=$date[4]->month;?></div></div></div>
                        <h4>Начало работы</h4>
                        <ul>
                            <li><span>Ваш автомобиль начинает выполнять заказы</span></li>
                            <li><span>Вы можете наблюдать за его передвижениями через специальное приложение</span></li>
                        </ul>
                    </div>
                </div>

                <div class="tr right">
                    <div class="td">
                        <div class="date-xs"><div class="date"><?=$date[5]->day;?> <div><?=$date[5]->month;?></div></div></div>
                        <h4>Первая ежемесячная выплата </h4>
                        <ul>
                            <li><span>Выплата первых процентов по договору</span></li>
                        </ul>
                    </div>
                    <div class="td hidden-xs">&nbsp;</div>
                </div>

                <div class="tr left">
                    <div class="td">&nbsp;</div>
                    <div class="td">&nbsp;</div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="leaveorder container">
    <div class="row">
        <div class="col-xs-12 padd0">
            <div class="leaveorder__wrap">
                <h2>Оставьте заявку, чтобы получить бесплатную консультацию нашего специалиста</h2>
                <p class="button__wrap"><a href="#modal_callback" data-fancybox class="button yellow">получить консультацию</a></p>
            </div>
        </div>
    </div>
</div>


<div class="faq" id="main_faq">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 paddR0">
                <h2>Ответы на часто задаваемые вопросы</h2>

                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                <h4 class="panel-title">Collapsible Group 1</h4>
                            </a>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                commodo consequat.</div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                <h4 class="panel-title">Collapsible Group 2</h4>
                            </a>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                commodo consequat.</div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                <h4 class="panel-title">Collapsible Group 3</h4>
                            </a>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                commodo consequat.</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="weare container">
    <div class="row">
        <div class="col-xs-12"><h2>Основа нашего развития - профессиональная команда</h2></div>
    </div>
    <div class="row">

<?php
$t='
        <div class="col-lg-4 col-sm-6 col-xs-12" style="margin-bottom: 30px;">
            <div class="weare__card">
                <p><img src="img/photo1.jpg" alt="Александр Сергеев"></p>
                <h4>Александр Сергеев</h4>
                <p class="weare__sub">Директор по развитию сети "Автофлот"</p>
                <ul>
                    <li><span>5 лет занимался развитием крупных проектов (Coffee Like, Что-то ещё)</span></li>
                    <li><span>Специализация - управление персоналом, что-то ещё здесь.</span></li>
                </ul>
                <p class="weare__ask-wrap"><span class="weare__ask">Задать вопрос Александру</span></p>
            </div>
        </div>
        
        
        <div class="col-lg-4 col-sm-6 col-xs-12" style="margin-bottom: 30px;">
            <div class="weare__card">
                <p><img src="img/photo2.jpg" alt="Майя Солнцева"></p>
                <h4>Майя Солнцева</h4>
                <p class="weare__sub">Менеджер по работе с инвесторами"</p>
                <ul>
                    <li><span>5 лет занимался развитием крупных проектов (Coffee Like, Что-то ещё)</span></li>
                    <li><span>Специализация - управление персоналом, что-то ещё здесь.</span></li>
                    <li><span>5 лет занимался развитием крупных проектов (Coffee Like, Что-то ещё)</span></li>
                    <li><span>Специализация - управление персоналом, что-то ещё здесь.</span></li>
                </ul>
                <p class="weare__ask-wrap"><span class="weare__ask">Задать вопрос Александру</span></p>
            </div>
        </div>
        
        
        <div class="col-lg-4 col-sm-6 col-xs-12" style="margin-bottom: 30px;">
            <div class="weare__card">
                <p><img src="img/photo3.jpg" alt="Сергей Смышляев"></p>
                <h4>Сергей Смышляев</h4>
                <p class="weare__sub">Директор технического отдела</p>
                <ul>
                    <li><span>5 лет занимался развитием крупных проектов (Coffee Like, Что-то ещё)</span></li>
                    <li><span>Специализация - управление персоналом, что-то ещё здесь.</span></li>
                </ul>
                <p class="weare__ask-wrap"><span class="weare__ask">Задать вопрос Александру</span></p>
            </div>
        </div>
';

for ($i=0; $i<2; $i++) {
    echo $t;
}

?>

    </div>
</div>


<!--<div class="map-container" id="main_region" style="padding: 35px 0 0 0; height: 640px; position: relative;">-->
<!--    <div class="map_place" style="height: 640px; width: 100%; position: absolute; left: 0; top: 0;"></div>-->
<!--</div>-->

<div class="map">

    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="map__form">
                    <h4>Остались вопросы?</h4>
                    <p>Задайте их нашему специалисту прямо сейчас</p>
                    <textarea name="" id="" cols="30" rows="10" placeholder="Текст вопроса"></textarea>
                    <input type="text" placeholder="Введите номер телефона">
                    <input type="submit" value="Задать вопрос" class="button">
                    <p class="map__small">Нажимая на кнопку «Задать вопрос», вы акцептуете <a href="">оферту</a> и соглашаетесь на обработку <a href="">персональных данных</a></p>
                </div>

            </div>
        </div>
    </div>

    <div class="map_place"></div>

</div>


<footer class="footer" id="main_contacts">
    <div class="container">
        <div class="row">


            <div class="col-sm-6 hidden-lg">
                <img src="img/logo.png" class="logo">
            </div>
            <div class="col-sm-6 hidden-lg">
                <div class="footer__phone"><a href="tel:<?=PHONE_LINK;?>"><?=PHONE;?></a></div>
                <div class="footer__phone-desc">(звонок по РФ бесплатный)</div>
            </div>
            <div class="col-xs-12 visible-xs">
                <ul>
                    <li><a href="">Расчет доходности</a></li>
                    <li><a href="">Гарантии</a></li>
                    <li><a href="">Схема работы</a></li>
                    <li><a href="">Ответы на вопросы</a></li>
                    <li><a href="">Контакты</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>




            <div class="col-lg-5 col-lg-push-0 col-sm-6 col-sm-push-6">
                <img src="img/logo.png" class="logo visible-lg">
                <p class="footer__small">ООО «Автофлот»<br>
                ИНН/КПП 6313548195/631301001<br>
                ОГРН 1156313000733<br>
                ОКПО 33553211<br>
                Россия, г. Ижевск, ул. Пушкинская 173а Бизнес Центр H7</p>
            </div>
            <div class="col-lg-2 col-lg-push-1 col-sm-6 col-sm-pull-6 hidden-xs">
                <ul class="footer__non-xs-ul">
                    <li><a href="">Расчет доходности</a></li>
                    <li><a href="">Гарантии</a></li>
                    <li><a href="">Схема работы</a></li>
                    <li><a href="">Ответы на вопросы</a></li>
                    <li><a href="">Контакты</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-lg-offset-2 visible-lg">
                <div class="footer__phone"><a href="tel:<?=PHONE_LINK;?>"><?=PHONE;?></a></div>
                <div class="footer__phone-desc">(звонок по РФ бесплатный)</div>
            </div>
        </div>
    </div>
</footer>

<?php include_once("modals.php"); ?>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.ui.touch-punch.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA95ad6EPsLdFTTDg5iw3-zFD23JUNI4eI"></script>
<script type="text/javascript" src="js/main_pack.js"></script>
<script src="js/common.js"></script>
<script src="js/map.js"></script>
<script src="js/app.js"></script>
</body>
</html>

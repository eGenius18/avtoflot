<?php
class ReferralTail
{
  private $inputs = null;

  public function renderInputs()
  {
    if ($this->inputs == null)
    {
      $utmInputs = $this->getUtmInputs();
      $refererInputs = $this->getRefererInputs();
      $inputs = array_merge($utmInputs, $refererInputs);
      $this->inputs = implode("\n", $inputs);
    }
    echo $this->inputs;
  }

  private function getUtmInputs()
  {
    $utmLabels = array(
        'utm_campaign' => 'Campaign',
        'utm_content' => 'Keyword'
    );
    $inputs = array();
    foreach ($utmLabels as $label => $prop)
    {
      list($value) = explode('/', $_GET[$label], 2);
      if (isset($value) && $value != '')
      {
        $inputs[] = "<input type=\"hidden\" name=\"{$prop}\" value=\"{$value}\" />";
      }
    }
    return $inputs;
  }

  private function getRefererInputs()
  {
    $referer = $_SERVER['HTTP_REFERER'];
    $inputs = array();
    if (isset($referer))
    {
      $sources = array(
          'cap' => 'q',
          'yandex' => 'text',
          'google' => 'q',
          'mail' => 'q',
          'rambler' => 'words',
          'bing' => 'q',
          'qip' => 'query'
      );

      $url = parse_url($referer);
      $domains = explode('.', $url['host']);
      $successful = false;
      foreach ($sources as $source => $param)
      {
        if (in_array($source, $domains))
        {
          $params = array();
          parse_str($url['query'], $params);
          $inputs[] = "<input type=\"hidden\" name=\"Source\" value=\"{$url['host']}\" />";
          $inputs[] = "<input type=\"hidden\" name=\"Query\" value=\"{$params[$param]}\" />";
          $successful = true;
          break;
        }
      }
      if (!$successful)
      {
        $referer = rawurldecode($referer);
        $inputs[] = "<input type=\"hidden\" name=\"Referer\" value=\"{$referer}\" />";
      }
    }
    return $inputs;
  }
}
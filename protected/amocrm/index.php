<?php
if(isset($_GET['task'])) {
    if('ajax' == $_GET['task']) {
        $company = udsRequest('company');
        if(isset($company['errorCode'])) die(json_encode(['error' => true, 'content' => 'UDS-Game company error: ' . $company['errorCode']]));

        if('' == $_POST['uds_code']) die(json_encode(['error' => false, 'content' => ['company' => $company]]));

        $customer = udsRequest('customer', ['code' => $_POST['uds_code']]);

        die(json_encode(['error' => false, 'content' => ['company' => $company, 'customer' => $customer]]));
    } elseif('uds-webhook' == $_GET['task']) {
        $AMO = array(
            'subdomain' => 'tobefit',
            'login' => 'tobe.comos@mail.ru',
            'key' => '3ae1d9bb79fae701feb057371128aaed',
        );

        if(isset($_POST['account']['subdomain']) && $AMO['subdomain'] == $_POST['account']['subdomain'] && isset($_POST['leads']) && is_array($_POST['leads'])) {
            require_once implode(DIRECTORY_SEPARATOR, array(__DIR__, 'vendor', 'autoload.php'));
            try {
                $amo = new \AmoCRM\Client($AMO['subdomain'], $AMO['login'], $AMO['key']);

                $amo->fields->Products = [
                    410019 => true, //Шоколад
                    410021 => true, //Ваниль
                    410023 => true, //Клубника
                ];
                $amo->fields->UDS = [
                    'code' => 553616,
                    'scores' => 553618
                ];

                foreach(['status', 'add'] as $status) {
                    if(isset($_POST['leads'][$status]) && is_array($_POST['leads'][$status])) {
                        foreach($_POST['leads'][$status] as $deal) {
                            $products = 0;
                            $udsData = [
                                'code' => '',
                                'scores' => 0,
                            ];
                            foreach($deal['custom_fields'] as $field) {
                                if(isset($amo->fields->Products[$field['id']])) {
                                    foreach($field['values'] as $qty) $products += intVal($qty['value']);
                                } elseif($amo->fields->UDS['code'] == $field['id']) $udsData['code'] = $field['values'][0]['value'];
                                elseif($amo->fields->UDS['scores'] == $field['id']) $udsData['scores'] = $field['values'][0]['value'];
                            }

                            $price = intVal($deal['price']);
                            $udsData['scores'] = intVal($udsData['scores']);
                            if(0 < ($price + $udsData['scores']) && 0 < $products) {
                                if('status' == $status && 142 == intVal($deal['status_id'])) {

                                    $lead = $amo->lead->apiList(['id' => $deal['id']]);
                                    $contact = $amo->contact->apiList(['id' => $lead[0]['main_contact_id']]);

                                    foreach($contact[0]['custom_fields'] as $field) {
                                        if(297555 == $field['id']) {
                                            $phone = array_shift($field['values']);
                                            $params = array(
                                                'sum' => $price + $udsData['scores'],
                                                'phone' => $phone['value'],
                                                'invoice' => $deal['id'],
                                            );
                                            break 1;
                                        }
                                    }
                                    $note = $amo->note;
                                    $note['element_id'] = $deal['id'];
                                    $note['element_type'] = 2;
                                    $note['note_type'] = 4;
                                    if(isset($params)) {
                                        $result = makeUdsPurchase($params);
                                        $note['text'] = $result['message'];
                                    } else $note['text'] = 'Сделка в UDS-Game:' . PHP_EOL . 'Не найден контакт AMO-CRM, на который оформлена сделка';
                                    $note->apiAdd();
                                } elseif('add' == $status) {
                                    $params = array(
                                        'sum' => $price,
                                        'udsCode' => $udsData['code'],
                                        'udsScores' => $udsData['scores'],
                                        'invoice' => $deal['id'],
                                    );
                                    $note = $amo->note;
                                    $note['element_id'] = $deal['id'];
                                    $note['element_type'] = 2;
                                    $note['note_type'] = 4;
                                    $result = makeUdsPurchase($params);
                                    $note['text'] = $result['message'];
                                    $note->apiAdd();

                                    $search = strtolower('Списано баллов:');
                                    $p = strpos(strtolower($result['message']), $search);
                                    $scores = 0;
                                    $lead = $amo->lead;
                                    if(false !== $p) {
                                        $scores = intVal(substr($result['message'], $p + strlen($search)));
                                        $p = $price - $scores;
                                        if($p < $price) $lead['price'] = $p;
                                    }
                                    $lead->addCustomField(553618, $scores);
                                    $lead->apiUpdate((int)$deal['id'], 'now');
                                }
                            }
                        }
                    }
                }
            } catch (\AmoCRM\Exception $e) {
                mailAmoError($e->getCode(), $e->getMessage());
            }
        }

        die('');
    }
}

function mailAmoError($code, $message) {
    require_once implode(DIRECTORY_SEPARATOR, array(__DIR__, '..', 'MailHelper.php'));
    foreach(['liketravelsellrise@mail.ru', 'tdsportsystems@gmail.com'] as $mail) {
        $mailHelper = new MailHelper();
        $mailHelper
            ->setTo($mail)
            ->setSubject('Ошибка обработки')
            ->setBody('При обработке вебхука возникла ошибка '.($code == '' ? ': '.$message : '('.$code.'): '.$message))
            ->send();
    }
    die('');
}

function makeUdsPurchase($params) {
    $res = [
        'error' => false,
        'message' => 'Сделка в UDS-Game:',
    ];
    $sum = empty($params['sum']) ? 0 : intVal($params['sum']);
    $company = udsRequest('company');
    if(!isset($company['errorCode'])) {
        if(empty($params['phone'])) {
            $scores = empty($params['udsScores']) ? 0 : intVal($params['udsScores']);
            if('' != $params['udsCode'] && 0 < $scores) {
                $customer = udsRequest('customer', ['code' => $params['udsCode']]);
                if (!isset($customer['errorCode'])) {
                    $baseDiscount = 'APPLY_DISCOUNT' == $company['baseDiscountPolicy'] ? $company['marketingSettings']->discountBase : 0;
                    $discount = $sum * $baseDiscount / 100;//Базовая скидка UDS-Game
                    $maxScores = floor(($sum - $discount) * $company['marketingSettings']->maxScoresDiscount / 100);
                    if ($scores > $maxScores) $scores = $maxScores;
                    if ($scores > $customer['scores']) $scores = $customer['scores'];
                    $cash = $sum - $discount - $scores;

                    $purchaseUDS = udsRequest('purchase', [], [
                        'code' => $params['udsCode'],
                        'total' => 0 == $cash ? $sum : $scores,
                        'scores' => $scores,
                        'cash' => 0,
                        'invoiceNumber' => $params['invoice'],
                    ]);

                    if(!isset($purchaseUDS['errorCode'])) $res['message'] .= PHP_EOL . 'Списано баллов: ' . $scores;
                    else {
                        $res['error'] = true;
                        $res['message'] .= PHP_EOL . 'Ошибка: (' . $purchaseUDS['errorCode'] . ') ' . $purchaseUDS['message'];
                        if(isset($purchaseUDS['errors'])) {
                            foreach($purchaseUDS['errors'] as $error) {
                                $res['message'] .= PHP_EOL . $error['field'] . ': ' . $error['message'] . '(' . $error['value'] . ')';
                            }
                        }
                    }
                } else {
                    $res['error'] = true;
                    $res['message'] .= PHP_EOL . 'Ошибка: (' . $customer['errorCode'] . ') ' . $customer['message'];
                }
            } else {
                $res['error'] = true;
                $res['message'] .= PHP_EOL . ('' == $params['udsCode'] ? 'Пользователь не использовал код' : 'Пользователь использовал код, но не тратил баллы');
            }
        } else {
            $purchaseUDS = udsRequest('purchase', [], [
                'total' => $sum,
                'scores' => 0,
                'cash' => $sum,
                'invoiceNumber' => $params['invoice'],
                'phone' => '+' . preg_replace('~\D+~','', $params['phone']),
            ]);

            if(!isset($purchaseUDS['errorCode'])) $res['message'] .= PHP_EOL . 'Сделка оформлена на телефонный номер: ' . $params['phone'];
            else {
                $res['error'] = true;
                $res['message'] .= PHP_EOL . 'Ошибка: (' . $purchaseUDS['errorCode'] . ') ' . $purchaseUDS['message'];
                if(isset($purchaseUDS['errors'])) {
                    foreach($purchaseUDS['errors'] as $error) {
                        $res['message'] .= PHP_EOL . $error['field'] . ': ' . $error['message'] . ' (' . $error['value'] . ')';
                    }
                }
            }
        }
    } else {
        $res['error'] = true;
        $res['message'] .= PHP_EOL . 'Ошибка: (' . $company['errorCode'] . ') ' . $company['message'];
    }

    return $res;
}

function udsRequest($method, $get = [], $post = []) {
    $udsKey = 'KzddbloyQ3dSfUBzZUJBXiRqX0VTV0ZkOEQjVW0tWGVwM0FhZlJpXXszRi04WkBVeX4=';
    $result = false;

    if($curl = curl_init()) {
        $url = 'https://udsgame.com/v1/partner/'.$method;
        if(is_array($get)) {
            $params = [];
            foreach ($get as $key => $value) $params[] = $key . '=' . $value;
            if (0 < count($params)) $url .= '?' . implode('&', $params);
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'X-Origin-Request-Id: '.md5(time()),
            'X-Timestamp: '.date( DATE_ISO8601 ),
            'X-Api-Key: '.$udsKey,
        ));
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if(is_array($post) && 0 < count($post)) curl_setopt($curl, CURLOPT_POSTFIELDS, $post);

        $result = get_object_vars(json_decode(curl_exec($curl)));

        if(isset($result['errors'])) {
            $err = [];
            foreach($result['errors'] as $error) {
                $err[] = get_object_vars($error);
            }
            $result['errors'] = $err;
        }

        curl_close($curl);
    }

    return $result;
}
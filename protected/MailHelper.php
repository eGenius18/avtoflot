<?php
class MailHelper
{
  private $Mailer;
  private $To;

  public function __construct()
  {
    require_once dirname(__FILE__) . '/mailer/PHPMailerAutoload.php';

    $this->Mailer = new PHPMailer();
    $this->Mailer->CharSet = 'utf-8';
    $this->Mailer->XMailer = 'mailer';
    $this->Mailer->FromName = 'Tobe-Fit';
    $this->Mailer->From = 'noreply@' . $_SERVER['SERVER_NAME'];
  }

  public function setTo($to)
  {
    if (is_array($to))
    {
      $this->To = $to;
    }
    else
    {
      $this->To = array($to);
    }
    return $this;
  }

  public function setSubject($subject)
  {
    $this->Mailer->Subject = $subject;
    return $this;
  }

  public function setBody($body)
  {
    $this->Mailer->Body = $body;
    return $this;
  }

  public function isHtml($isHtml = true)
  {
    $this->Mailer->isHTML($isHtml);
    return $this;
  }

  public function send()
  {
    foreach ($this->To as $to)
    {
      $this->Mailer->addAddress($to);
      $this->Mailer->send();
      $this->Mailer->clearAllRecipients();
    }
  }
}
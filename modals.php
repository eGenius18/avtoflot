<div id="modal_return" style="display: none;">
    <div class="table">
        <div class="tr">
            <div class="td">
                <h4>Досрочный возврат инвестиций</h4>
                <p>Начиная с 6 месяца в договоре предусмотрен досрочный вывод средств. Вы ничем не рискуете так как все автомобили застрахованы банком</p>
            </div>
            <div class="td">
                <h5>Остались вопросы?</h5>
                <p>Получите консультацию нашего специалиста</p>
                <form method="post" action="" class="post-form modal-window">
                    <input type="hidden" name="Contact" value="1" />
                    <input type="hidden" name="Goal" value="calculator" />
                    <label><input type="text" name="Name" class="modal-input" placeholder="Введите ваше имя"></label>
                    <label><input type="text" name="Phone" class="modal-input" placeholder="Введите номер телефона"></label>
                    <button type="submit" class="modal-btn button"><span class="btn-text">Задать вопрос</span></button>
                    <div class="modal-info">Нажимая на кнопку «Задать вопрос», вы акцептуете <a href="">оферту</a> и соглашаетесь на обработку <a href="">персональных данных</a></div>
                </form>
                <div class="post-success form-hidden"><div class="modal-success">Ваша заявка успешно отправлена.<br>Мы свяжемся с вами в ближайшее время.</div></div>
            </div>
        </div>
    </div>
</div>


<div id="modal_callback" style="display: none;">
    <div class="modal-wrap">
        <h4>Получить консультацию специалиста</h4>
        <p>В течение 5 минут с вами свяжется специалист и ответит на все интересующие вопросы</p>
        <form method="post" action="" class="post-form modal-window">
        <div class="table">
            <div class="tr">
                <div class="td">
                    <input type="hidden" name="Contact" value="1" />
                    <input type="hidden" name="Goal" value="callback" />
                    <label><input type="text" name="Name" class="modal-input" placeholder="Введите ваше имя"></label>
                    <button type="submit" class="modal-btn button"><span class="btn-text">получить консультацию</span></button>
                </div>
                <div class="td">
                        <label><input type="text" name="Phone" class="modal-input" placeholder="Введите номер телефона"></label>
                </div>
            </div>
        </div>
        <div class="modal-info">Нажимая на кнопку «Получить консультацию», вы акцептуете <a href="">оферту</a> и соглашаетесь на обработку <a href="">персональных данных</a></div>
        </form>
        <div class="post-success form-hidden"><div class="modal-success">Ваша заявка успешно отправлена.<br>Мы свяжемся с вами в ближайшее время.</div></div>
    </div>
</div>

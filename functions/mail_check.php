<?php
if (isset($_POST['contact']))
{
  $valid = array(
      'name'  => array('/[A-Za-zА-Яа-яЁё\s]/', 'Введите имя'),
      'phone' => array('/^((8|\+7)[\- ]?)?(\(?\d{3,4}\)?[\- ]?)?[\d\- ]{5,10}$/', 'Введите телефон'),
      'email' => array('/^[-_a-z0-9\'+*$^&%=~!?{}]++(?:\.[-_a-z0-9\'+*$^&%=~!?{}]+)*+@(?:(?![-.])[-a-z0-9.]+(?<![-.])\.[a-z]{2,6}|\d{1,3}(?:\.\d{1,3}){3})(?::\d++)?$/iD', 'Введите email')
  );

  $errors = array();

  foreach ($valid as $field => $data)
  {
    if (isset($_POST[$field]))
    {
      $input = trim($_POST[$field]);

      $regex   = $data[0];
      $message = $data[1];

      if (empty($input) || !preg_match($regex, $input))
      {
        $errors += array($field => $message);
      }
    }
  }

  $result = empty($errors) ? 'success' : 'errors';

  if ($result === 'success')
  {
    require_once $_SERVER['DOCUMENT_ROOT'].'/protected/post.php';
  }

  echo json_encode(array
      (
      'result' => $result,
      'errors' => $errors
  ));
  exit();
}
?>
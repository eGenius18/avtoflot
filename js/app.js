(function(){
$(document).ready(function () {

    // $(".compare__table2 #accordion").collapse("hide");


    $(".rowOpener").on("click",function (e) {
        e.preventDefault();
        var rowToOpen = $(this).attr('data-row');

        var parent = $(this).parent().parent();
        $(parent).children("div").each(function (i,e) {
            if (i != 0) {
                console.log($(this).children(".row").css('display'));
                var va = $(this).children(".row").css('display') === 'none' ? 'top' : 'middle';
                $(this).css({'vertical-align':va});
            }
        });
        $("."+rowToOpen).slideToggle();
    });




    function accordeonArrows() {
        $(".faq .panel").each(function () {
            var $this = $(this);
            var h4 = $this.find("h4");
            $this.find(".panel-collapse").hasClass("in") ? h4.addClass("arrow") : h4.removeClass("arrow");
        });
    }
    accordeonArrows();

    $('#accordion').on('shown.bs.collapse', function() {
        accordeonArrows();
    });




    //same height
    function sameHeightVer2() {
        var containerWidth = $(".container").width();
        var numOfElements;
        var shift = 50;

        switch (containerWidth) {
            case 320: numOfElements = 1; break;
            case 610: numOfElements = 2; break;
            default: numOfElements = 3; break;
        }

        $(".weare__card").css({height:'auto'}); //reset prev heights sets

        if (numOfElements != 1) {

            var numberOfRows = $(".weare__card").length / numOfElements;
            var rStart = 0, rEnd = numOfElements;

            for (var i = 0; i <= numberOfRows; i++) {
                var maxHeight = 0;
                var obj = $(".weare__card").slice(rStart, rEnd);

                obj.each(function () {
                    maxHeight = ($(this).height() > maxHeight) ? $(this).height() : maxHeight;
                });
                obj.height(maxHeight);

                rStart += numOfElements;
                rEnd += numOfElements;
            }
        }
    }
    sameHeightVer2();
    $(window).resize(function () {
        sameHeightVer2();
        // console.log($(".container").width());
    });





    $( "#slider1" ).slider({
        range: "min",
        min: 1,
        max: 10,
        orientation: "horizontal",
        value: 1,
        // values: [ 10, 200 ],
        change: function( event, ui ) {
            // filterTable()
        },
        slide: function( event, ui ) {
            recalc(ui.value,'cars');
        }
    });


    $( "#slider2" ).slider({
        range: "min",
        min: 6,
        max: 36,
        orientation: "horizontal",
        value: 36,
        // values: [ 10, 200 ],
        change: function( event, ui ) {
            // filterTable()
        },
        slide: function( event, ui ) {
            recalc(ui.value,'months');
        }
    });


    function spacer(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
    // для float чисел
    // function spacer(x) {
    //     var parts = x.toString().split(".");
    //     parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //     return parts.join(".");
    // }


    var prices = [
        0,
        0,
        0,
        0,
        0,
        0,
        572567.57,
        555423.42,
        538972.97,
        523216.22,
        508153.15,
        493783.78,
        480108.11,
        467126.13,
        454837.84,
        443243.24,
        432342.34,
        422135.14,
        412621.62,
        403801.80,
        395675.68,
        388243.24,
        381504.50,
        375459.46,
        370108.11,
        365450.45,
        361486.49,
        358216.22,
        355639.64,
        353756.76,
        352567.57,
        352072.07,
        352270.27,
        353162.16,
        354747.75,
        357027.03,
        360000.00
    ];

    //дефолтные и глобальные значения - их правим и из них берем цифры
    var globalVals = {
        'cars':             1,      //кол-во машин
        'months':           36,      //срок инвестиций
        'investSum':        690000, //Сумма ваших инвестиций
        'monthlyIncome':    22000,  //Ежемесячный доход
        'totalIncome':      264000, //Доход за X месяцев
        'payBack':          572000,  //Возврат после продажи машин
        'sum':              828000   //ВАШ ДОХОД
    };

    function recalc(val,slider) {

        var sklon;
        if (slider === 'cars') {
            switch (val) {
                case 1:
                    sklon = 'автомобиль';
                    break;
                case 2:
                case 3:
                case 4:
                    sklon = 'автомобиля';
                    break;
                default:
                    sklon = 'автомобилей';
            }
        } else {
            switch (val) {
                case 21:
                case 31:
                    sklon = 'месяц';
                    break;
                case 22:
                case 23:
                case 24:
                case 32:
                case 33:
                case 34:
                    sklon = 'месяца';
                    break;
                default:
                    sklon = 'месяцев';
            }
        }

        var where = slider === 'cars' ? '#slider1-out' : '#slider2-out';
        var price = 690000; //цена одной машины

        slider === 'cars' ? globalVals.cars = val : globalVals.months = val;

        globalVals.totalIncome = 22000 * globalVals.cars * globalVals.months;
        globalVals.payBack = Math.round(prices[globalVals.months] * globalVals.cars);
        globalVals.sum = globalVals.totalIncome + globalVals.payBack;
        globalVals.investSum = globalVals.cars * price;
        globalVals.monthlyIncome = globalVals.cars * 22000;

        if (slider === 'cars') {
            $(".num1").text(globalVals.cars);
            $(".sklon_auto").text(sklon);
            $(".sum1").text(spacer(globalVals.cars * price));
        } else {
            $(".num2").text(globalVals.months);
            $(".sklon_months").text(sklon);

            $("#dohod33").text(globalVals.months);
            $(".sklon_months2").text(sklon);

            val >= 12 ? $(".calc__return").slideUp() : $(".calc__return").slideDown();
        }

        $("#dohod1").text(spacer(globalVals.investSum));
        $("#dohod2").text(spacer(globalVals.monthlyIncome));
        $("#dohod3").text(spacer(globalVals.totalIncome));
        $("#dohod4").text(spacer(globalVals.payBack));
        $("#dohod5").text(spacer(globalVals.sum));

    }










});

}());
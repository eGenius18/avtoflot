$.fn.toggleButton = function (text, disabledText) {
    var $this = $(this);

    if ($this.attr('disabled')) {
        $this.html(text).removeAttr('disabled');
    }
    else {
        $this.html(disabledText).attr('disabled', 'disabled');
    }

    return this;
};

function yaReachGoal(goal) {
    try {
        yaCounter11111111.reachGoal(goal);
    }
    catch (e) {
        console.log("Метрика не установлена. Цель: ", goal);
    }
}

$(function() {
    $(document).on("submit", "form", function(e) {
            e.preventDefault();
            var $form = $(this),
                data = $form.serialize();

            var btn_text =  $form.find(":submit").html();

            $form.filter(".post-form").find(":submit").toggleButton(btn_text, "11Отправка...");



            $.post($form.attr("action"), data, function(json) {
                if (json.result === "success") {

                    yaReachGoal($form.find("[name=Goal]").val());
                    $form
                        .filter(".post-form")
                        .fadeOut(function() {
                            $(this).siblings(".post-success").fadeIn("slow");
                            $(this).remove();
                        });
                } else {
                    $form.filter(".post-form").find(":submit").toggleButton(btn_text, "22Отправка...");

                    $('div.errortxt').remove();
                    $(':input.error').removeClass('error');

                    $.each(json.errors, function(field, message) {
                        // Стандартные формы на странице
                        if ($form.is(".post-form")) {
                            $form.find("[name=" + field + "]").addClass('error').attr("placeholder", message);
                        }
                    });
                }
            }, "json");
        });

    $("[data-fancybox]").fancybox({
        // Options will go here
    });

});


// Плавный скролл
$(document).ready(function(){
    $( 'a[href^=\\#main]' ).click( function () {
        var id = $( this ).attr( 'href' ).match( /#.+$/ )[0].substr(1);
        var offsetTop = $("div[id='"+ id + "']").offset().top;
        $('body,html').animate({scrollTop:offsetTop}, 900);
        return false;
    });
});
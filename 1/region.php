<div class="map-container" id="main_region">
	<div class="map_place"></div>
	<?php if (isset($landing['region_title'])) { ?><div class="std-header"><?php echo $landing['region_title'] ?></div><?php } ?>
	<?php if (isset($landing['region_description'])) { ?><div class="std-desc"><?php echo $landing['region_description'] ?></div><?php } ?>
	<div class="map_filter">
		<div class="map_filter_icon active" id="mf-0" data-targ="0"><span>Здоровье</span></div>
		<div class="map_filter_icon active" id="mf-1" data-targ="1"><span>Спорт и фитнес</span></div>
		<div class="map_filter_icon active" id="mf-2" data-targ="2"><span>Магазины</span></div>
		<div class="map_filter_icon active" id="mf-3" data-targ="3"><span>Детские сады</span></div>
		<div class="map_filter_icon active" id="mf-5" data-targ="5"><span>Обучение</span></div>
		<div class="map_filter_icon active" id="mf-6" data-targ="6"><span>Транспорт</span></div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){	
	// Подгрузка XML
	var infra_xml;
	function test_infra_xml(url,targ_function) {
		if (!infra_xml) {
			var type;
			if(/json/.test(url)) {
				type='json';
			}
			if(type) {
				$.ajax({
					url: url,
					dataType: type,
					success: function (response) {
						//console.log(response);
						infra_xml = [];
						var i = 1;
						if(type=='json') {
							var hrefs = {
								'cnst#pin_medicine': 0,
								'cnst#pin_sport': 1,
								'cnst#pin_shop': 2,
								'cnst#pin_childs': 3,
								'cnst#pin_relax': 4,
								'cnst#pin_educate': 5,
								'cnst#pin_transport': 6
							};
							$.each(response,function(index,value) {
								var point_coord = value.geometry.coordinates;
								console.log(value.options.preset);
								infra_xml.push([point_coord[1], point_coord[0], value.properties.name, value.properties.description, hrefs[value.options.preset], value.options.preset, i++]);
							});
						}
						targ_function();
					}
				});
			}
		} else {
			targ_function();
		}
	}
	console.log('x');

	// Карты
	function gmapsMarker(options){
		var obj_image = new google.maps.MarkerImage(
			'/img/xxx/map/house.png',
			new google.maps.Size(53,59),
			new google.maps.Point(0,0)
		);
		return obj_marker = new google.maps.Marker({
			position: new google.maps.LatLng(<?php echo $landing['region_house_coord'] ?>),
			map: options.map,
			icon: obj_image,
			clickable: false
		});
	}
	function load_infrastructure_map(points,groups,test) {
		console.log(infra_xml);
		var obj_point = new google.maps.LatLng(<?php echo $landing['region_map_coord'] ?>);
		var myOptions = {
			zoom: 15,
			center: obj_point,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			scrollwheel: false,
			zoomControl: true,
			zoomControlOptions: {
			  style: google.maps.ZoomControlStyle.LARGE,
			  position: google.maps.ControlPosition.RIGHT_CENTER
			}
		};
		var map = new google.maps.Map(document.getElementById('map_place'), myOptions);

		var obj_marker=new gmapsMarker({
			//map: map,
			position: obj_point
		});

		var markers={};
		var bounds=new google.maps.LatLngBounds();
		bounds.extend(obj_marker.getPosition());
		var icons_colors=['#e3000f','#ffa423','#1ec135','#0090ff','#a214d1','#df1989','#1cbead'];
		function get_icon_style(type,hover) {
			var img;
			img = new google.maps.MarkerImage(
				'/img/xxx/map/'+type+'.png',
				new google.maps.Size(34,40),
				new google.maps.Point(0,0)
			);
			return img;
		}
		for (var i=0; i<points.length; i++) {
			var icon=points[i];
			var type=Number(icon[4]);
			var marker = new MarkerWithLabel({
				position: new google.maps.LatLng(icon[0],icon[1]),
				title: points[i][2],
				map: map,
				icon: get_icon_style(type,false),
				type: type,
				labelContent: '<span class="gmaps_labels_span n'+type+'"><span style="background-color:'+icons_colors[type]+'">'+icon[2]+'</span></span>',
				labelClass: 'gmaps_labels',
				labelVisible: false,
			});
			if(!markers[type]) {
				markers[type]=[];
			}
			markers[type].push(marker);
			bounds.extend(marker.getPosition());
			google.maps.event.addListener(marker,'mouseover',function() {
				this.setOptions({icon:get_icon_style(this.type,true),labelVisible:true,zIndex:10000});
			});
			google.maps.event.addListener(marker,'mouseout',function() {
				this.setOptions({icon:get_icon_style(this.type,false),labelVisible:false,zIndex:null});
			});
		}
		
		var marker_obj = new MarkerWithLabel({
			position: new google.maps.LatLng(<?php echo $landing['region_house_coord'] ?>),
			map: map,
			icon: new google.maps.MarkerImage(
				'/img/xxx/map/house.png',
				new google.maps.Size(53,59),
				new google.maps.Point(0,0)
			)

		});
		markers[9]=[marker_obj];


		$('.map_filter_icon').bind('click',function(){
			$(this).toggleClass('active');
			var visible=$(this).hasClass('active');
			var group_num=$(this).data('targ');
			$.each(markers[group_num],function(index,value){
				value.setVisible(visible);
			});
		});
		if(test) {
			$.each(markers,function(index,value){
				var txt=index+': ';
				$.each(value,function(index2,value2){
					txt+=value2.title+', ';
				});
				txt=null;
			})
		}
	}
	
	$('.map_place').attr('id','map_place');
	test_infra_xml('http://ogni18.ru/js/json/ogni18.json', function(){
		load_infrastructure_map(infra_xml,'all',true);
	});

});
</script>